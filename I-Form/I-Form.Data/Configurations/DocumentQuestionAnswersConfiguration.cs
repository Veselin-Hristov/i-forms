﻿using I_Form.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Data.Configurations
{
    public class DocumentQuestionAnswersConfiguration : IEntityTypeConfiguration<DocumentQuestionAnswers>
    {
        public void Configure(EntityTypeBuilder<DocumentQuestionAnswers> builder)
        {
             builder.HasKey(dqa => new { dqa.ResponseId, dqa.QuestionId });

            builder.Property(dqa => dqa.Answer).IsRequired();

        }
    }
}
