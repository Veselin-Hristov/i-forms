﻿using I_Form.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Data.Configurations
{
    public class FormConfiguration : IEntityTypeConfiguration<Form>
    {
        public void Configure(EntityTypeBuilder<Form> builder)
        {
            builder.HasKey(f => f.Id);

            builder.Property(f => f.Title)
                .IsRequired();

            builder.Property(f => f.Description)
                       .IsRequired();

            builder.HasOne(f => f.Author)
                .WithMany(a => a.Forms)
                .HasForeignKey(f => f.AuthorId);
        }
         
    }
}
