﻿using I_Form.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Data.Configurations
{
    public class OptionsQuestionAnswersConfiguration : IEntityTypeConfiguration<OptionsQuestionAnswers>
    {
        public void Configure(EntityTypeBuilder<OptionsQuestionAnswers> builder)
        {
         builder.HasKey(oqa => new { oqa.ResponseId, oqa.QuestionId });

            builder.Property(oqa => oqa.Answer).IsRequired();
        }
    }
}
