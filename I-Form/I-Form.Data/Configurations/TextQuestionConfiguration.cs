﻿using I_Form.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Data.Configurations
{
    public class TextQuestionConfiguration : IEntityTypeConfiguration<TextQuestion>
    {
        public void Configure(EntityTypeBuilder<TextQuestion> builder)
        {
            builder.HasKey(tq => tq.Id);

            builder.HasOne(tq => tq.Form)
                .WithMany(f => f.TextQuestions)
                .HasForeignKey(tq => tq.FormId);

            builder.Property(tq => tq.Title)
                .IsRequired();

     builder.Property(tq => tq.Description)
                .IsRequired();

        }
    }
}
