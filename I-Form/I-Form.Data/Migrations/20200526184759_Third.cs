﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace I_Form.Data.Migrations
{
    public partial class Third : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("85f89774-e390-488b-8286-828d3c352c19"));

            migrationBuilder.DeleteData(
                table: "TextQuestions",
                keyColumn: "Id",
                keyValue: new Guid("221da8c0-027a-4478-901b-e0ff6fa7b12c"));

            migrationBuilder.DeleteData(
                table: "Forms",
                keyColumn: "Id",
                keyValue: new Guid("a1d638ab-2237-4327-87ae-6c6ea492dd03"));

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "CreatedOn", "DeletedOn", "Email", "EmailConfirmed", "IsDeleted", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UpdatedOn", "UserName" },
                values: new object[] { new Guid("d5af3240-0e23-48b2-87fd-9402aee20701"), 0, "68d955e6-23ca-407f-a87d-446cb67d37bc", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, false, false, null, null, null, null, null, false, null, false, null, "TestUser1" });

            migrationBuilder.InsertData(
                table: "Forms",
                columns: new[] { "Id", "AuthorId", "CreatedOn", "DeletedOn", "Description", "IsDeleted", "Title", "UpdatedOn" },
                values: new object[] { new Guid("178a2dcd-93cb-4583-88f0-7afd41c77abe"), null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Test Description", false, "Test Form", null });

            migrationBuilder.InsertData(
                table: "TextQuestions",
                columns: new[] { "Id", "CreatedOn", "DeletedOn", "Description", "FormId", "IsDeleted", "IsLongAnswer", "IsRequired", "Title", "UpdatedOn" },
                values: new object[] { new Guid("13cd2ceb-d198-4b67-b480-b21a025cf3ba"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Test Description", new Guid("178a2dcd-93cb-4583-88f0-7afd41c77abe"), false, false, false, "Test Title", null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("d5af3240-0e23-48b2-87fd-9402aee20701"));

            migrationBuilder.DeleteData(
                table: "TextQuestions",
                keyColumn: "Id",
                keyValue: new Guid("13cd2ceb-d198-4b67-b480-b21a025cf3ba"));

            migrationBuilder.DeleteData(
                table: "Forms",
                keyColumn: "Id",
                keyValue: new Guid("178a2dcd-93cb-4583-88f0-7afd41c77abe"));

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "CreatedOn", "DeletedOn", "Email", "EmailConfirmed", "IsDeleted", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UpdatedOn", "UserName" },
                values: new object[] { new Guid("85f89774-e390-488b-8286-828d3c352c19"), 0, "a4ed2b89-7b10-4c9a-a621-780f46cdb039", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, null, false, false, false, null, null, null, null, null, false, null, false, null, "TestUser1" });

            migrationBuilder.InsertData(
                table: "Forms",
                columns: new[] { "Id", "AuthorId", "CreatedOn", "DeletedOn", "Description", "IsDeleted", "Title", "UpdatedOn" },
                values: new object[] { new Guid("a1d638ab-2237-4327-87ae-6c6ea492dd03"), null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Test Description", false, "Test Form", null });

            migrationBuilder.InsertData(
                table: "TextQuestions",
                columns: new[] { "Id", "CreatedOn", "DeletedOn", "Description", "FormId", "IsDeleted", "IsLongAnswer", "IsRequired", "Title", "UpdatedOn" },
                values: new object[] { new Guid("221da8c0-027a-4478-901b-e0ff6fa7b12c"), new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Test Description", new Guid("a1d638ab-2237-4327-87ae-6c6ea492dd03"), false, false, false, "Test Title", null });
        }
    }
}
