﻿using I_Form.Data.Abstract.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Data.Abstract
{
    public interface IEntity 
    {
        public Guid Id { get; set; }
public string Title { get; set; }
    }

}
