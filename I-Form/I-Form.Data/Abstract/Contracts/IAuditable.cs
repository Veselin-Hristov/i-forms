﻿using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Data.Abstract.Contracts
{
   public interface IAuditable
    {
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}
