﻿using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Data.Abstract.Contracts
{
   public interface IDeletable
    {
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; } 

    }
}
