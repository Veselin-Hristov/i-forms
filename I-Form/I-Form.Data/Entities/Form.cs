﻿using I_Form.Data.Abstract;
using I_Form.Data.Abstract.Contracts;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using System.Text;

namespace I_Form.Data.Entities
{
    public class Form : IEntity, IDeletable, IAuditable
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public ICollection<TextQuestion> TextQuestions { get; set; }
        public ICollection<OptionsQuestion> OptionsQuestions { get; set; }
        public ICollection<DocumentQuestion> DocumentQuestions { get; set; }
        public User Author { get; set; }
        public Guid? AuthorId { get; set; }
        public ICollection<Responses> Responses { get; set; }

        public Form()
        {
            TextQuestions = new List<TextQuestion>();
        }
    }
}
