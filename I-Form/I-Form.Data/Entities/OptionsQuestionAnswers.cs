﻿using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Data.Entities
{
    public class OptionsQuestionAnswers
    {
        public Guid QuestionId { get; set; }
        public OptionsQuestion Question { get; set; }
        public Guid ResponseId { get; set; }
        public Responses Responses { get; set; }
        public string Answer { get; set; }
    }
}
