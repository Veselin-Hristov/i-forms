﻿using I_Form.Data.Abstract.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Data.Entities
{
    public class Option: IAuditable,IDeletable
    {
        public Guid Id { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }

        public Guid? QuestionId { get; set; }
        public OptionsQuestion Question { get; set; }
        public string Text { get; set; }
    }
}
