﻿using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Data.Entities
{
    public class DocumentQuestionAnswers
    {
        public Guid QuestionId { get; set; }
        public DocumentQuestion Question { get; set; }
        public Responses Responses { get; set; }
        public Guid ResponseId { get; set; }
        public string Answer { get; set; }
    }
}
