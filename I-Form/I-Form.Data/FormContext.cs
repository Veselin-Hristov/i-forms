﻿using I_Form.Data.Configurations;
using I_Form.Data.Entities;
using I_Form.Data.Seeder;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Data
{
    public class FormContext : IdentityDbContext<User,Role,Guid>
    {
        public FormContext() { }


        public FormContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Form> Forms { get; set; }
        public DbSet<Option> Options { get; set; }
        public DbSet<OptionsQuestion> OptionsQuestions { get; set; }
        public DbSet<TextQuestion> TextQuestions { get; set; }
        public DbSet<DocumentQuestion> DocumentQuestions { get; set; }
        public DbSet<OptionsQuestionAnswers> OptionsQuestionAnswers { get; set; }
        public DbSet<TextQuestionAnswers> TextQuestionAnswers { get; set; }
        public DbSet<DocumentQuestionAnswers> DocumentQuestionAnswers { get; set; }
        public DbSet<Responses> Responses { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new DocumentQuestionAnswersConfiguration());
            builder.ApplyConfiguration(new OptionsQuestionAnswersConfiguration());
            builder.ApplyConfiguration(new TextQuestionAnswersConfiguration());
            builder.ApplyConfiguration(new TextQuestionConfiguration());
            builder.ApplyConfiguration(new OptionsConfiguration());
            builder.ApplyConfiguration(new OptionsQuestionConfiguration());
            builder.ApplyConfiguration(new DocumentQuestionConfiguration());
            builder.ApplyConfiguration(new ResponsesConfiguration());
            builder.ApplyConfiguration(new FormConfiguration());

            builder.Seeder();

            base.OnModelCreating(builder);
        }
    }
}
