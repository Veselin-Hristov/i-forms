﻿using AutoMapper;
using I_Form.Models;
using I_Form.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace I_Form.Mappers.Profiles
{
    public class TextQuestionViewModelProfile : Profile
    {
        public TextQuestionViewModelProfile()
        {
            CreateMap<TextQuestionDTO, TextQuestionViewModel>();
            CreateMap<TextQuestionDTO, TextQuestionViewModel>().ReverseMap();

        }

    }
}
