﻿using AutoMapper;
using I_Form.Models;
using I_Form.Services.DTOEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace I_Form.Mappers.Profiles
{
    public class FormViewModelProfile : Profile
    {
        public FormViewModelProfile()
        {
            CreateMap<FormDTO, FormViewModel>();
            CreateMap<FormDTO, FormViewModel>().ReverseMap();
           
        }
    }
}
