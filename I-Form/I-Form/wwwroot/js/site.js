﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.



$(function () {
    var PlaceHolderElement = $('#PlaceHolderHere2');

    $('button[data-toggle="ajax-modal"]').ready().click(function (event) {


        var url = $(this).data('url');
        $.get(url).done(function (data) {
            PlaceHolderElement.html(data);
            PlaceHolderElement.find('.modal').modal('show');
        })


        PlaceHolderElement.on('click', '[data-save="modal"]', function (event) {
            var optionQuestion = $(this).parents('.modal').find('form');
            var actionUrl = optionQuestion.attr('action');
            var sendData = optionQuestion.serialize();
            let formId = $('#idButton2').attr('data-formid2');

            //var isMultyChoice = ($('#modalCheckLongA').is(':checked'));

            sendData += `&FormId=${formId}`;
            //sendData += `&isMultyChoice=${isMultyChoice}`;

            let modalTitle = $('#modalTitle').val();
            let modalDesc = $('#modalDesc').val();

            console.log(modalTitle + modalDesc);

            //$('#txtQC').html(`<h1>${modalTitle}</h1><br>`)
            //$('#txtQC').html(`<p>${modalDesc}</p>`)



            $.post(actionUrl, sendData).done(function (data) {
                PlaceHolderElement.find('.modal').modal('hide')
            })
        });
    })
})
