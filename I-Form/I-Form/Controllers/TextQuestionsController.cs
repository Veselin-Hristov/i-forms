﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using I_Form.Data.Entities;
using I_Form.Models;
using I_Form.Services.Contracts;
using I_Form.Services.DTOs;
using Microsoft.AspNetCore.Mvc;

namespace I_Form.Controllers
{
    public class TextQuestionsController : Controller
    {
        private readonly IMapper _mapper;

        private readonly ITextQuestionService _txtQService;

        private readonly IFormService _formService;

        public TextQuestionsController(IMapper mapper, ITextQuestionService txtQService, IFormService formService)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _txtQService = txtQService ?? throw new ArgumentNullException(nameof(txtQService));
            _formService = formService ?? throw new ArgumentNullException(nameof(formService));
        }

        [HttpGet]
        //Get : TextQuestions
        public async Task<IActionResult> Index()
        {
            var txtQs = await _txtQService.GetAllTextQuestionAsync();

            var txtQViewModels = _mapper.Map<List<TextQuestionViewModel>>(txtQs);

            return View(txtQViewModels);
        }

        [HttpGet]
        //Get : TextQuestions/Details/1
        public async Task<IActionResult> Details(Guid id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var txtQ = await _txtQService.GetTextQuestionAsync(id);

            var txtQViewModel = _mapper.Map<FormViewModel>(txtQ);

            if (txtQ == null)
            {
                return NotFound();
            }

            return View(txtQViewModel);
        }

        [HttpGet]
        public IActionResult CreateTextQuestion()
        {
            var textQ = new TextQuestionViewModel();
            return PartialView("_TextQuestionPartial",textQ);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateTextQuestion(TextQuestionViewModel textQuestionViewModel)
        {

            var txtQDTO = _mapper.Map<TextQuestionDTO>(textQuestionViewModel);

            await _txtQService.CreateTextQuestionAsync(txtQDTO);

            var txtQVM = _mapper.Map<TextQuestionViewModel>(txtQDTO);

            

            return View("Details");
        }
        [HttpGet]
        public async Task<IActionResult> Edit(Guid Id)
        {
            var question = await this._txtQService.GetTextQuestionAsync(Id);
            return PartialView("_TextQuestionPartial", question);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public async Task<IActionResult> Edit(Guid Id, TextQuestionViewModel txtQViewModel)
        {
            if (Id != txtQViewModel.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                await _txtQService.UpdateTextQuestionAsync(Id, txtQViewModel.Title, txtQViewModel.Description);
                return RedirectToAction(nameof(Index));
            }

            return View(txtQViewModel);
        }
    }
}