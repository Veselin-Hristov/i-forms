﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using I_Form.Data;
using I_Form.Data.Entities;
using I_Form.Models;
using AutoMapper;
using I_Form.Services.Contracts;
using System.Security.Policy;
using Azure;

namespace I_Form.Controllers
{
    public class ResponsesController : Controller
    {
        private readonly FormContext _context;
        private readonly IMapper mapper;
        private readonly IResponsesService responsesService;
        private readonly ITextQuestionService textQuestionService;

        public ResponsesController(FormContext context,IMapper mapper,IResponsesService responsesService,ITextQuestionService textQuestionService)
        {
            _context = context;
            this.mapper = mapper;
            this.responsesService = responsesService;
            this.textQuestionService = textQuestionService;
        }

        // GET: Responses
        public async Task<IActionResult> Index()
        {
            var formContext = _context.Responses.Include(r => r.Form);
            return View(await formContext.ToListAsync());
        }

        [HttpGet]
        public async Task<IActionResult> Submit(Guid id)
        {
            var responseDTO = await this.responsesService.GetAsync(id);
            var textQDTO = await this.textQuestionService.GetAllTextQuestionAsync(id);
            var textQVM = mapper.Map<List<TextQuestionViewModel>>(textQDTO);
            var responseVM = mapper.Map<ResponsesViewModel>(responseDTO);
            responseVM.TextQuestionsVM = textQVM;
                       return View(responseVM);
        }

        [HttpPost]
        public async Task<IActionResult> Submit(ResponsesViewModel responseVM)
        {
            var response = new Responses();
            response.FormId = responseVM.FormId;
            response.CreatedOn = DateTime.Now;
            response.TextQuestionAnswers = responseVM.TextQuestionsVM.Select(tq => new TextQuestionAnswers()
            {
                Answer = tq.Content
            }).ToList();

            await this._context.Responses.AddAsync(response);
            await this._context.SaveChangesAsync();

            TempData["SuccessMessage"] = "The response has been submitted successfully!";

            return RedirectToAction(nameof(Submit));
        }

        // GET: Responses/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var responses = await _context.Responses
                .Include(r => r.Form)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (responses == null)
            {
                return NotFound();
            }

            return View(responses);
        }

        // GET: Responses/Create
        public IActionResult Create()
        {
            ViewData["FormId"] = new SelectList(_context.Forms, "Id", "Description");
            return View();
        }

        // POST: Responses/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,FormId,CreatedOn")] Responses responses)
        {
            if (ModelState.IsValid)
            {
                responses.Id = Guid.NewGuid();
                _context.Add(responses);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["FormId"] = new SelectList(_context.Forms, "Id", "Description", responses.FormId);
            return View(responses);
        }

        // GET: Responses/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var responses = await _context.Responses.FindAsync(id);
            if (responses == null)
            {
                return NotFound();
            }
            ViewData["FormId"] = new SelectList(_context.Forms, "Id", "Description", responses.FormId);
            return View(responses);
        }

        // POST: Responses/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Id,FormId,CreatedOn")] Responses responses)
        {
            if (id != responses.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(responses);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ResponsesExists(responses.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["FormId"] = new SelectList(_context.Forms, "Id", "Description", responses.FormId);
            return View(responses);
        }

        // GET: Responses/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var responses = await _context.Responses
                .Include(r => r.Form)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (responses == null)
            {
                return NotFound();
            }

            return View(responses);
        }

        // POST: Responses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var responses = await _context.Responses.FindAsync(id);
            _context.Responses.Remove(responses);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ResponsesExists(Guid id)
        {
            return _context.Responses.Any(e => e.Id == id);
        }
    }
}
