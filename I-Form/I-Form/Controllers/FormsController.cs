﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using I_Form.Data;
using I_Form.Data.Entities;
using I_Form.Services.Contracts;
using AutoMapper;
using I_Form.Models;
using I_Form.Services.DTOEntities;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Authorization;
using I_Form.Services.DTOs;
using System.Security.Claims;

namespace I_Form.Controllers
{
    [AutoValidateAntiforgeryToken]
    public class FormsController : Controller
    {
        private readonly FormContext _context;

            private readonly IMapper _mapper;

            private readonly IFormService _formService;
            private readonly ITextQuestionService _txtQService;

        public FormsController(IFormService formService, IMapper mapper, ITextQuestionService txtQService)
        {
            _formService = formService;
            _mapper = mapper;
            _txtQService = txtQService;
        }

        // GET: Forms
        public async Task<IActionResult> Index()
        {
            var formDTO = await _formService.GetAllFormsAsync();

            var formViewModel = _mapper.Map<List<FormViewModel>>(formDTO);

            return View(formViewModel);
        }

        // GET: Forms/Details/5
        public async Task<IActionResult> Details(Guid id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var form = await _formService.GetFormAsync(id);

            var formViewModel = _mapper.Map<FormViewModel>(form);

            if (form == null)
            {
                return NotFound();
            }

            return View(formViewModel);
        }

        // GET: Forms/Create
        public IActionResult Create()
        {
            var form = new FormViewModel();
            
            return View(form);
        }

        [HttpGet]
        public IActionResult CreateTextQuestion()
        {
            var txtQ = new TextQuestionViewModel();
            return PartialView("_TextQuestionPartial", txtQ);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(FormViewModel form)
        {
            if (ModelState.IsValid)
            {

                var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);

                var id = new Guid(userId);

                form.AuthorId = id;

                var formDTO = _mapper.Map<FormDTO>(form);
                await _formService.CreateFormAsync(formDTO);
                var formVM = _mapper.Map<FormViewModel>(formDTO);

                return View("Details",formVM);
            }
           
            return View(form);
        }

   



        // GET: Forms/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var form = await _context.Forms.FindAsync(id);
            if (form == null)
            {
                return NotFound();
            }
            ViewData["AuthorId"] = new SelectList(_context.Users, "Id", "Id", form.AuthorId);
            return View(form);
        }

        // POST: Forms/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("Id,Title,Description,DeletedOn,IsDeleted,CreatedOn,UpdatedOn,AuthorId")] Form form)
        {
            if (id != form.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(form);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FormExists(form.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AuthorId"] = new SelectList(_context.Users, "Id", "Id", form.AuthorId);
            return View(form);
        }

        // GET: Forms/Delete/5
        public async Task<IActionResult> Delete(Guid id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var form = await _formService.GetFormAsync(id);

            if (form == null)
            {
                return NotFound();
            }

            var formVM = _mapper.Map<FormViewModel>(form);

            return View(formVM);
        }

        // POST: Forms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var form = await _formService.DeleteFormAsync(id);
            return RedirectToAction(nameof(Index));
        }

        private bool FormExists(Guid id)
        {
            return _context.Forms.Any(e => e.Id == id);
        }
    }
}
