﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace I_Form.Models
{
        public class DocumentContentModel
    {
        public Stream Content { get; set; }

        public string FileName { get; set; }
    }
}
