﻿using I_Form.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace I_Form.Models
{
    public class ResponsesViewModel
    {
        public Guid Id { get; set; }
        public Guid FormId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public Form Form { get; set; }
        public ICollection<DocumentQuestionAnswers> DocumentQuestionAnswers { get; set; }
        public ICollection<TextQuestionAnswers> TextQuestionAnswers{ get; set; }
        public ICollection<OptionsQuestionAnswers> OptionsQuestionAnswers{ get; set; }
        public List<TextQuestionViewModel> TextQuestionsVM{ get; set; }

    }
}
