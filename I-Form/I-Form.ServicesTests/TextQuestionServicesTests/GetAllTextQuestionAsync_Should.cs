﻿using AutoMapper;
using I_Form.Data;
using I_Form.Data.Entities;
using I_Form.Services;
using I_Form.Services.DTOs;
using I_Form.Services.Providers.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Form.ServicesTests.TextQuestionServicesTests
{
    [TestClass]
    public class GetAllTextQuestionAsync_Should
    {
        IMapper _mapper;
        [TestMethod]

        public async Task Return_DTO_Collection_If_Successful()
        {
            var options = TestUtils.GetOptions(nameof(Return_DTO_Collection_If_Successful));
            var mockDateTime = new Mock<IDateTimeProvider>();

            var id = Guid.NewGuid();
            var id2 = Guid.NewGuid();

            var textQ = new TextQuestion
            {
                Id = id,
                Title = "TestTitle1"
            };
            var textQ2 = new TextQuestion
            {
                Id = id2,
                Title = "TestTitle2"
            };
            var textQDTO1 = new TextQuestionDTO
            {
                Id = id,
                Title = "TestTitle1"
            };
            var textQ2DTO2 = new TextQuestionDTO
            {
                Id = id2,
                Title = "TestTitle2"
            };

            using (var arrangeContext = new FormContext(options))
            {
                await arrangeContext.TextQuestions.AddAsync(textQ);
                await arrangeContext.TextQuestions.AddAsync(textQ2);
                await arrangeContext.SaveChangesAsync();
            }

            using (var assertContext = new FormContext(options))
            {
                var sut = new TextQuestionService(assertContext, mockDateTime.Object, _mapper);
                var resultDTOs = await sut.GetAllTextQuestionAsync();

                resultDTOs.ToList();

                List<TextQuestionDTO> expected = new List<TextQuestionDTO>(2);
                expected.Add(textQDTO1);
                expected.Add(textQ2DTO2);

                Assert.AreEqual(resultDTOs, expected);
            }

        }
    }
}
