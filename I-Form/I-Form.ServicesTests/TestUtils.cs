using I_Form.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace I_Form.ServicesTests
{
    
    public class TestUtils
    {
        public static DbContextOptions<FormContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<FormContext>()
                .UseInMemoryDatabase(databaseName)
                .Options;
        }
    }
}
