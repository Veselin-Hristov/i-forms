﻿using I_Form.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Services.DTOEntities
{
    public class DocumentQuestionDTO
    {
         public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Guid FormId { get; set; }
        public string Form { get; set; }
        public ICollection<DocumentQuestionAnswers> DocumentQuestionAnswers { get; set; }
        public bool IsRequired { get; set; }
        public int FileNumberLimit { get; set; }
        public int FileSizeLimit { get; set; }

    }
}
