﻿using I_Form.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Services.DTOEntities
{
    public class OptionDTO
    {
        public Guid Id { get; set; }

        public Guid QuestionId { get; set; }
        public OptionsQuestion Question { get; set; }
        public string Text { get; set; }

    }
}
