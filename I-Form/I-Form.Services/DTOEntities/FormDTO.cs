﻿using I_Form.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Services.DTOEntities
{
    public class FormDTO
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public ICollection<TextQuestionDTO> TextQuestions { get; set; }
        public ICollection<OptionsQuestionDTO> OptionsQuestions { get; set; }
        public ICollection<DocumentQuestionDTO> DocumentQuestions { get; set; }
        public string Author { get; set; }
        public Guid AuthorId { get; set; }
        public ICollection<ResponsesDTO> Responses { get; set; }
    }
}
