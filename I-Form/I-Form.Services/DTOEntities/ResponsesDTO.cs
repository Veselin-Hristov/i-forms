﻿using I_Form.Data.Entities;
using I_Form.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Services.DTOEntities
{
    public class ResponsesDTO
    {
        public Guid Id { get; set; }
        public Guid FormId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public Form Form { get; set; }
        public ICollection<DocumentQuestionAnswers> DocumentQuestionAnswers { get; set; }
        public ICollection<TextQuestionAnswers> TextQuestionAnswers { get; set; }
        public ICollection<OptionsQuestionAnswers> OptionsQuestionAnswers { get; set; }
        public ICollection<TextQuestionDTO> TextQuestionsVM { get; set; }

    }
}
