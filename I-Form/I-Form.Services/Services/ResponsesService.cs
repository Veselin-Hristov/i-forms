﻿using I_Form.Data;
using I_Form.Services.Contracts;
using I_Form.Services.DTOEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Form.Services.Services
{
    public class ResponsesService : IResponsesService
    {
        private readonly FormContext _context;

        public ResponsesService(FormContext context)
        {
            this._context = context;
        }
        public Task<ResponsesDTO> CreateAsync(ResponsesDTO responsesDTO)
        {
            throw new NotImplementedException();
        }

        public Task<ICollection<ResponsesDTO>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public async Task<ResponsesDTO> GetAsync(Guid id)
        {
            var form = await _context.Forms.FindAsync(id);
            var textQuestions = _context.TextQuestions.Where(tq => tq.FormId == id);
            var responseDTO = new ResponsesDTO
            {
                Title = form.Title,
                Description = form.Description,
                FormId = form.Id,
            };
            return responseDTO;
        }
    }
}
