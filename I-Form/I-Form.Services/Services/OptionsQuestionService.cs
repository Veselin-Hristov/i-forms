﻿using AutoMapper;
using I_Form.Data;
using I_Form.Data.Entities;
using I_Form.Services.Contracts;
using I_Form.Services.DTOEntities;
using I_Form.Services.Mappers;
using I_Form.Services.Providers.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace I_Form.Services.Services
{
    public class OptionsQuestionService : IOptionsQuestionService
    {
        private readonly FormContext _context;
        private readonly IDateTimeProvider _dateTimeProvider;
        private readonly IMapper _mapper;
      

        public OptionsQuestionService(FormContext context,IDateTimeProvider dateTimeProvider,IMapper mapper)
        {
            _context = context;
            _dateTimeProvider = dateTimeProvider;
            _mapper = mapper;
        }

        public async Task<OptionsQuestionDTO> CreateAsync(OptionsQuestionDTO optionsQuestionDTO)
        {
            if (await this._context.OptionsQuestions.AnyAsync(oq => oq.Id == optionsQuestionDTO.Id && oq.IsDeleted == false))
            {
                throw new ArgumentNullException();
            }
            var optionQuestionAnswer = this._context.OptionsQuestionAnswers;
            var id = Guid.NewGuid();

            var optionsQuestion = new OptionsQuestion
            {
                Id = id,
                Title = optionsQuestionDTO.Title,
                Description = optionsQuestionDTO.Description,
            };

            await this._context.OptionsQuestions.AddAsync(optionsQuestion);
            await this._context.SaveChangesAsync();

            optionsQuestionDTO.Id = id;
            return optionsQuestionDTO;
        }

        public async Task<OptionDTO> CreateOptionAsync(OptionDTO optionsDTO)
        {
            if (await this._context.Options.AnyAsync(o => o.Id == optionsDTO.Id && o.IsDeleted == false))
            {
                throw new ArgumentNullException();
            }
            var id = Guid.NewGuid();

            var option = new Option
            {
                Id = id,
                Text = optionsDTO.Text,
            };

            await this._context.Options.AddAsync(option);
            await this._context.SaveChangesAsync();

            optionsDTO.Id = id;
            return optionsDTO;
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            try
            {
                var optionQuestion = this._context.OptionsQuestions
                                    .Where(oQ => oQ.IsDeleted == false)
                                    .FirstOrDefault(oQ => oQ.Id == id);
                //TODO: Add await 

                optionQuestion.IsDeleted = true;
                optionQuestion.FormId = null;

                optionQuestion.DeletedOn = this._dateTimeProvider.GetDateTime();
                await this._context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<bool> DeleteOptionAsync(Guid id)
        {
                   try
            {
                var option = this._context.Options
                                    .Where(o => o.IsDeleted == false)
                                    .FirstOrDefault(o => o.Id == id);
                //TODO: Add await 

                option.IsDeleted = true;
                option.QuestionId = null;

                option.DeletedOn = this._dateTimeProvider.GetDateTime();
                await this._context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<OptionsQuestionDTO> EditAsync(Guid id, OptionsQuestionDTO optionsQuestionDTO)
        {
            var optionQuestion = await this._context.OptionsQuestions
                .Where(oQ => oQ.IsDeleted == false)
                .FirstOrDefaultAsync(oQ => oQ.Id == id);
            //TODO: Should we take DTO or different variables

            optionQuestion.Title = optionsQuestionDTO.Title;
            optionQuestion.Description = optionsQuestionDTO.Description;
            optionQuestion.UpdatedOn = _dateTimeProvider.GetDateTime();

            await _context.SaveChangesAsync();

            var optionQuestionDTO = _mapper.Map<OptionsQuestionDTO>(optionQuestion);

            return optionQuestionDTO;
        }

        public async Task<OptionDTO> EditOptionAsync(Guid id, OptionDTO optionsDTO)
        {
            var option = await this._context.Options
                .Where(oQ => oQ.IsDeleted == false)
                .FirstOrDefaultAsync(oQ => oQ.Id == id);
            //TODO: Should we take DTO or different variables

            option.Text = optionsDTO.Text;
            option.UpdatedOn = this._dateTimeProvider.GetDateTime();

            await this._context.SaveChangesAsync();

        
            var optionDTO = _mapper.Map<OptionDTO>(option);

            return optionDTO;
        }

        public async Task<ICollection<OptionsQuestionDTO>> GetAllAsync(Guid formId)
        {
            var optionsQuestions = await this._context.OptionsQuestions
                .Where(oQ => oQ.IsDeleted == false && oQ.FormId == formId)
                .ToListAsync();

            //TODO: After auto mapper is added map textQuestions to DTO and return it



            var optionsQuestionDTOs = _mapper.Map<ICollection<OptionsQuestionDTO>>(optionsQuestions);
            //TODO: ICollection or List for mapper?

            return optionsQuestionDTOs;
        }

        public async Task<ICollection<OptionDTO>> GetAllOptionsAsync(Guid questionID)
        {
            //TODO: Should we use id?
            var options = await this._context.Options
                .Where(oQ => oQ.IsDeleted == false && oQ.QuestionId == questionID)
                .ToListAsync();

            //TODO: After auto mapper is added map textQuestions to DTO and return it

           

            var optionsDTO = _mapper.Map<ICollection<OptionDTO>>(options);
            //TODO: ICollection or List for mapper?

            return optionsDTO;
        }

        public async Task<OptionDTO> GetOptionAsync(Guid id)
        {
              var option =await this._context.Options
                .Where(o => o.IsDeleted == false)
                .FirstOrDefaultAsync(o => o.Id == id);
            //TODO: Add await
            if(option == null)
            {
                throw new ArgumentNullException();
            }

       

            var optionDTO = _mapper.Map<OptionDTO>(option);

            return optionDTO;
        }

        public async Task<OptionsQuestionDTO> GetOptionsQuestion(Guid id)
        {
            var optionQuestion =await this._context.OptionsQuestions
                .Where(oq => oq.IsDeleted == false)
                .FirstOrDefaultAsync(oq => oq.Id == id);
            //TODO: Add await
            if(optionQuestion == null)
            {
                throw new ArgumentNullException();
            }

          

            var optionsQuestionDTO = _mapper.Map<OptionsQuestionDTO>(optionQuestion);

            return optionsQuestionDTO;
        }
    }
}
