﻿using AutoMapper;
using I_Form.Data;
using I_Form.Data.Entities;
using I_Form.Services.Contracts;
using I_Form.Services.DTOEntities;
using I_Form.Services.Mappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Form.Services.Services
{
    public class DocumentQuestionService : IDocumentQuestionService
    {
             private readonly FormContext _context;
        private readonly IMapper _mapper;

        public DocumentQuestionService(FormContext context,IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<DocumentQuestionDTO> CreateAsync(DocumentQuestionDTO documentQuestionDTO)
        {
            if (await this._context.DocumentQuestions.AnyAsync(dq => dq.Id == documentQuestionDTO.Id && dq.IsDeleted == false))
            {
                throw new ArgumentNullException();
            }
            var documentQuestionAnswer = _context.DocumentQuestionAnswers;
            var id = Guid.NewGuid();

            var documentQuestion = new DocumentQuestion
            {
                Id = id,
                Title = documentQuestionDTO.Title,
                Description = documentQuestionDTO.Description,
            };

            await _context.DocumentQuestions.AddAsync(documentQuestion);
            await _context.SaveChangesAsync();

            documentQuestionDTO.Id = id;
            return documentQuestionDTO;
        }

       
        public async Task<bool> DeleteAsync(Guid id)
        {
            try
            {
                var documentQuestion = _context.DocumentQuestions
                                    .Where(dq => dq.IsDeleted == false)
                                    .FirstOrDefault(dq => dq.Id == id);
                //TODO: Add await 

                documentQuestion.IsDeleted = true;
                documentQuestion.FormId = null;

                documentQuestion.DeletedOn = DateTime.UtcNow;
                await _context.SaveChangesAsync();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

       
        public async Task<DocumentQuestionDTO> EditAsync(DocumentQuestionDTO documentQuestionDTO)
        {
            var documentQuestion = await _context.DocumentQuestions
                .Where(dq => dq.IsDeleted == false)
                .FirstOrDefaultAsync(dq => dq.Id == documentQuestionDTO.Id);

            documentQuestion.Title = documentQuestionDTO.Title;
            documentQuestion.Description = documentQuestionDTO.Description;
            documentQuestion.UpdatedOn = DateTime.UtcNow;

            await _context.SaveChangesAsync();

            return documentQuestionDTO;
        }

       
        public async Task<ICollection<DocumentQuestionDTO>> GetAllAsync(Guid formId)
        {
            var documentQuestions = await _context.DocumentQuestions
                .Where(dq => dq.IsDeleted == false && dq.FormId == formId)
                .ToListAsync();

            var documentQuestionsDTOs = _mapper.Map<ICollection<DocumentQuestionDTO>>(documentQuestions);
            //TODO: ICollection or List for mapper?

            return documentQuestionsDTOs;
        }

       
       
        public async Task<DocumentQuestionDTO> GetAsync(Guid id)
        {
            var documentQuestion =await _context.DocumentQuestions
                .Where(dq => dq.IsDeleted == false)
                .FirstOrDefaultAsync(dq => dq.Id == id);
            if(documentQuestion == null)
            {
                throw new ArgumentNullException();
            }

       

            var documentQuestionDTO = _mapper.Map<DocumentQuestionDTO>(documentQuestion);

            return documentQuestionDTO;
        }
    }
}
