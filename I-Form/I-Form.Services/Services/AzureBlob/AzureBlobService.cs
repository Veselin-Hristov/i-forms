﻿using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using AzureBlobLearning.Services;
using I_Form.Services.Providers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Azure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace I_Form.Services.Services
{
	public class AzureBlobService{

   
		private readonly IAzureBlobConnectionFactory _azureBlobConnectionFactory;
		private readonly BlobServiceClient blobServiceClient;

		public AzureBlobService(IAzureBlobConnectionFactory azureBlobConnectionFactory,BlobServiceClient blobServiceClient)
	{
		_azureBlobConnectionFactory = azureBlobConnectionFactory;
			this.blobServiceClient = blobServiceClient;
		}

	public async Task DeleteAllAsync()
	{
		var blobContainer = await _azureBlobConnectionFactory.GetBlobContainer();

		BlobContinuationToken blobContinuationToken = null;
		do
		{
			var response = await blobContainer.ListBlobsSegmentedAsync(blobContinuationToken);
			foreach (IListBlobItem blob in response.Results)
			{
				if (blob.GetType() == typeof(CloudBlockBlob))
					await ((CloudBlockBlob)blob).DeleteIfExistsAsync();
			}
			blobContinuationToken = response.ContinuationToken;
		} while (blobContinuationToken != null);
	}

	public async Task DeleteAsync(string fileUri)
	{
		var blobContainer = await _azureBlobConnectionFactory.GetBlobContainer();

		Uri uri = new Uri(fileUri);
		string filename = Path.GetFileName(uri.LocalPath);

		var blob = blobContainer.GetBlockBlobReference(filename);
		await blob.DeleteIfExistsAsync();
	}

	public async Task<IEnumerable<Uri>> ListAsync()
	{
		var blobContainer = await _azureBlobConnectionFactory.GetBlobContainer();
		var allBlobs = new List<Uri>();
		BlobContinuationToken blobContinuationToken = null;
		do
		{
			var response = await blobContainer.ListBlobsSegmentedAsync(blobContinuationToken);
			foreach (IListBlobItem blob in response.Results)
			{
				if (blob.GetType() == typeof(CloudBlockBlob))
					allBlobs.Add(blob.Uri);
			}
			blobContinuationToken = response.ContinuationToken;
		} while (blobContinuationToken != null);
		return allBlobs;
	}

	public async Task UploadAsync(IFormFileCollection files)
	{
		var blobContainer = await _azureBlobConnectionFactory.GetBlobContainer();

		for (int i = 0; i < files.Count; i++)
		{
			var blob = blobContainer.GetBlockBlobReference(GetRandomBlobName(files[i].FileName));
			using (var stream = files[i].OpenReadStream())
			{
				await blob.UploadFromStreamAsync(stream);

			}
		}
	}
		public async Task UploadContentBlobAsync(Stream content, string fileName)
        {

			var containerClient = blobServiceClient.GetBlobContainerClient("fileupload");
                    var blobClient = containerClient.GetBlobClient(fileName);

            await blobClient.UploadAsync(content, new BlobHttpHeaders { ContentType = fileName.GetContentType() });
        }

	/// <summary> 
	/// string GetRandomBlobName(string filename): Generates a unique random file name to be uploaded  
	/// </summary> 
	private string GetRandomBlobName(string filename)
	{
		string ext = Path.GetExtension(filename);
		return string.Format("{0:10}_{1}{2}", DateTime.Now.Ticks, Guid.NewGuid(), ext);
	}
	}
}



