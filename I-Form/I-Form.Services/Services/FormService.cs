﻿using AutoMapper;
using I_Form.Data;
using I_Form.Data.Entities;
using I_Form.Services.Contracts;
using I_Form.Services.DTOEntities;
using I_Form.Services.Mappers;
using I_Form.Services.Providers.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Form.Services.Services
{
    public class FormService : IFormService
    {
        private readonly IDateTimeProvider _dateTimeProvider;
        private readonly FormContext _context;
        private readonly IMapper _mapper;

        public FormService(IDateTimeProvider dateTimeProvider, FormContext context,IMapper mapper)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
            _dateTimeProvider = dateTimeProvider ?? throw new ArgumentNullException(nameof(dateTimeProvider));
            _mapper = mapper;
        }

        public async Task<FormDTO> CreateFormAsync(FormDTO formDTO)
        {
            var check = await _context.Forms
                   .FirstOrDefaultAsync(tQ => tQ.Title == formDTO.Title);

            if (check != null)
            {
                throw new ArgumentException("Form with the same Tittle already exists!");
            }

            

            var id = Guid.NewGuid();

            formDTO.Id = id;

            var form = _mapper.Map<Form>(formDTO);
          

            await _context.Forms.AddAsync(form);
            await _context.SaveChangesAsync();


            return formDTO;
        }

        public async Task<bool> DeleteFormAsync(Guid id)
        {
            try
            {
                var textQuestion = await this._context.Forms
                    .Where(tQ => tQ.IsDeleted == false)
                    .FirstOrDefaultAsync(tQ => tQ.Id == id);

                textQuestion.IsDeleted = true;
              

                textQuestion.DeletedOn = this._dateTimeProvider.GetDateTime();
                await this._context.SaveChangesAsync();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async Task<ICollection<FormDTO>> GetAllFormsAsync()
        {
            var form = await this._context.Forms
               .Include(a => a.Author)
               .Include(tQ => tQ.TextQuestions)
               .Where(f=> f.IsDeleted == false)
               .ToListAsync();

            //TODO: After auto mapper is added map textQuestions to DTO and return it

            //var config = new AutoMapperConfiguration().Configure();
            //var mapper = config.CreateMapper();

            var formDTOs = _mapper.Map<List<FormDTO>>(form);

            return formDTOs;
        }

        public async Task<FormDTO> GetFormAsync(Guid id)
        {
            var form = await this._context.Forms
                 .Where(f => f.IsDeleted == false)
                 .Include(tQ => tQ.TextQuestions)
                 .Include(f => f.Author)
                 .FirstOrDefaultAsync(tq => tq.Id == id);

            if (form.Equals(null))
            {
                throw new ArgumentNullException("Text question does not exist!");
            }

            var formDTO = _mapper.Map<FormDTO>(form);

            return formDTO;

        }

        public async Task<FormDTO> UpdateFormAsync(Guid id, string newTitle, string newDescription)
        {
            var form = await this._context.Forms
              .Where(tQ => tQ.IsDeleted == false)
              .FirstOrDefaultAsync(tQ => tQ.Id == id);

            form.Title = newTitle;
            form.Description = newDescription;
            form.UpdatedOn = this._dateTimeProvider.GetDateTime();

            await this._context.SaveChangesAsync();


            var formDTO = _mapper.Map<FormDTO>(form);

            return formDTO;
        }
    }
}
