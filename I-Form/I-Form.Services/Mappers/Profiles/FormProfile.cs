﻿using AutoMapper;
using I_Form.Data.Entities;
using I_Form.Services.DTOEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Services.Mappers.Profiles
{
    public class FormProfile : Profile
    {
        public FormProfile()
        {
            CreateMap<Form, FormDTO>();
            CreateMap<Form, FormDTO>().ReverseMap(); ;
        }
    }
}
