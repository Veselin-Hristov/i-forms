﻿using AutoMapper;
using I_Form.Data.Entities;
using I_Form.Services.DTOEntities;
using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Services.Mappers.Profiles
{
    public class OptionsQuestionProfile : Profile
    {
        public OptionsQuestionProfile()
        {
            CreateMap<OptionsQuestion, OptionsQuestionDTO>().ReverseMap();
            CreateMap<Option, OptionDTO>().ReverseMap();
            CreateMap<ICollection<OptionsQuestion>, ICollection<OptionsQuestionDTO>>().ReverseMap();
            CreateMap<ICollection<Option>, ICollection<OptionDTO>>().ReverseMap();
        }

    }
}
