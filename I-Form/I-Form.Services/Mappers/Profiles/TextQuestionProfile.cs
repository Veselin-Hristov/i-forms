﻿using AutoMapper;
using I_Form.Data.Entities;
using I_Form.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Services.Mappers.Profiles
{
    public class TextQuestionProfile : Profile
    {
        public TextQuestionProfile()
        {
            CreateMap<TextQuestion, TextQuestionDTO>();
            CreateMap<TextQuestion, TextQuestionDTO>().ReverseMap();

        }

    }
}
