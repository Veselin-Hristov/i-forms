﻿using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Services.Providers.Contracts
{
    public interface IDateTimeProvider
    {
        DateTime GetDateTime();
    }
}
