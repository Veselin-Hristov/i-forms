﻿using I_Form.Services.Providers.Contracts;
using System;
using System.Collections.Generic;
using System.Text;

namespace I_Form.Services.Providers
{
    public class DateTimeProvider : IDateTimeProvider
    {
        public DateTime GetDateTime() => DateTime.UtcNow;
    }
}
