﻿using I_Form.Data.Entities;
using I_Form.Services.DTOEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace I_Form.Services.Contracts
{
    public interface IOptionsQuestionService
    {
        Task<OptionsQuestionDTO> GetOptionsQuestion(Guid id);
        Task<ICollection<OptionsQuestionDTO>> GetAllAsync(Guid formId);
        Task<OptionsQuestionDTO> CreateAsync(OptionsQuestionDTO optionsQuestionDTO);
        Task<bool> DeleteAsync(Guid id);
        Task<OptionsQuestionDTO> EditAsync(Guid id,OptionsQuestionDTO optionsQuestionDTO);
        Task<OptionDTO> CreateOptionAsync(OptionDTO optionsDTO);
        Task<OptionDTO> EditOptionAsync(Guid id, OptionDTO optionsDTO);
        Task<bool> DeleteOptionAsync(Guid id);
        Task<OptionDTO> GetOptionAsync(Guid id);
        Task<ICollection<OptionDTO>> GetAllOptionsAsync(Guid questionId);
    }
}
