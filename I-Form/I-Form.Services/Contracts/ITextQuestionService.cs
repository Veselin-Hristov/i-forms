﻿using I_Form.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace I_Form.Services.Contracts
{
    public interface ITextQuestionService
    {
        Task<TextQuestionDTO> GetTextQuestionAsync(Guid id);
        Task<ICollection<TextQuestionDTO>> GetAllTextQuestionAsync();
        Task<ICollection<TextQuestionDTO>> GetAllTextQuestionAsync(Guid formId);
        Task<TextQuestionDTO> CreateTextQuestionAsync(TextQuestionDTO textQuestionDTO);
        Task<TextQuestionDTO> UpdateTextQuestionAsync(Guid id,string newTitle, string newDescription);
        Task<bool> DeleteTextQuestionAsync(Guid id);
    }
}
