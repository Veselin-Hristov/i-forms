﻿using I_Form.Services.DTOEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace I_Form.Services.Contracts
{
    public interface IFormService
    {
        Task<FormDTO> GetFormAsync(Guid id);
        Task<ICollection<FormDTO>> GetAllFormsAsync();
        Task<FormDTO> CreateFormAsync(FormDTO formDTO);
        Task<FormDTO> UpdateFormAsync(Guid id, string newTitle, string newDescription);
        Task<bool> DeleteFormAsync(Guid id);
    }
}
