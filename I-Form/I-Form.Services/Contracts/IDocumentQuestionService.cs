﻿using I_Form.Services.DTOEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace I_Form.Services.Contracts
{
    public interface IDocumentQuestionService
    {
        Task<DocumentQuestionDTO> GetAsync(Guid id);
        Task<ICollection<DocumentQuestionDTO>> GetAllAsync(Guid questionId);
        Task<DocumentQuestionDTO> CreateAsync(DocumentQuestionDTO documentQuestionDTO);
        Task<DocumentQuestionDTO> EditAsync(DocumentQuestionDTO documentQuestionDTO);
        Task<bool> DeleteAsync(Guid id);

    }
}
