# I-Forms

Project description

I-Forms is website where users can create forms, share them and review the answers.

The application consists of:

· public part (accessible without authentication)

· private part (available for registered users)


The public part is visible without authentication and also consists of the login page, register page and answer form page.


The private part (which is for registered users only) is accessible after successful login.

The private part consists of:

· Forms list page

· Form – questions page

· Form – answers page

· Profile page